from Bot.config import Config
from Bot.quoter import Quoter
import json

CONFIG_DIR = "config/"

if __name__ == "__main__":
    with open(CONFIG_DIR + 'config.json', 'r') as json_data:
        d = json.load(json_data)
    config = Config(d)
    with open(CONFIG_DIR + 'help.txt', 'r') as help_text:
        config.set_help_text(help_text.read())

    bot = Quoter(config)
    bot.run()
