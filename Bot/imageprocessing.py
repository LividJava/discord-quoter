import io
from PIL import Image, ImageOps


class ImageProcessing:
    @staticmethod
    def get_image(data: io.BytesIO):
        return Image.open(data)

    @staticmethod
    def open_image(path: str):
        return Image.open(path)

    @staticmethod
    def resize(image: Image, new_width: int, new_height: int):
        return image.resize((new_width, new_height))

    @staticmethod
    def apply_mask(image: Image, mask: Image):
        mask = mask.convert('L')
        output = ImageOps.fit(image, mask.size, centering=(0.5, 0.5))
        output.putalpha(mask)
        return output

    @staticmethod
    def get_bytes(image: Image):
        buf = io.BytesIO()
        image.save(buf, format='PNG')
        return buf.getvalue()
