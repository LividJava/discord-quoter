class Config:
    def __init__(self, config):
        self._config = config
        self.command_prefix = self._config["command_prefix"]
        self.token = self._config["token"]
        self.reply_text = self._config["reply_text"]
        self.help = ""

    def set_help_text(self, text: str):
        self.help = text