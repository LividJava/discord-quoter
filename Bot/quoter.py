import re

import aiohttp
from discord import Client
import discord
from Bot.config import Config
from Bot.utils import Utils


class Quoter:
    def __init__(self, config):
        self._config = config  # type: Config
        self._client = Client()

        @self._client.event
        async def on_ready():
            await self.on_ready()

        @self._client.event
        async def on_message(msg):
            # don't respond to ourselves
            if msg.author == self._client.user:
                return
            await self.on_message(msg)

    def run(self):
        self._client.run(self._config.token)

    async def on_ready(self):
        print('Logged on as', self._client.user)

    @staticmethod
    def mention_user(user):
        """
        :param user: discord.Member
        :return: str
        """
        return "<@" + str(user.id) + ">"

    async def get_message(self, channel_id: str, message_id: str):
        channel = self._client.get_channel(int(channel_id))
        return await channel.fetch_message(message_id)

    async def get_message_from_link(self, link: str):
        parts = link.split('/')
        return await self.get_message(parts[-2], parts[-1])

    @staticmethod
    def shorten_content(content: str) -> str:
        return content.replace(" ", "").replace("\n", "").replace(">", "")

    async def get_message_from_content(self, channel: discord.TextChannel, ignore: discord.Message,
                                       content: str, mentions: [str]):
        content = self.shorten_content(content)
        async for message in channel.history(limit=200):  # type:discord.Message
            if message.author != self._client.user and not message.content.startswith(self._config.command_prefix) and\
                    message.id != ignore.id and message.author.id in mentions:
                msg_content = self.shorten_content(message.content)
                if msg_content == content:
                    return message

    @staticmethod
    async def get_message_from_user(channel: discord.TextChannel, ignore: discord.Message, user_id: str):
        async for message in channel.history(limit=200):  # type:discord.Message
            if message.id != ignore.id and message.author.id == user_id:
                return message

    @staticmethod
    def remove_first_mention(content: str) -> str:
        result = re.search(r'<(.*?)>', content)
        return content[0:result.start()] + content[result.end():]

    async def on_message(self, msg: discord.Message):
        content = msg.content  # type: str
        if msg.content.startswith(self._config.command_prefix):
            content = content[len(self._config.command_prefix):]
            content = content.lstrip()

            if content == "help":
                await msg.channel.send(self._config.help)
                return

            if content.startswith("> "):
                # this means it's a quote using discord's quote system
                # first separate by line
                lines = content.replace("\r", "\n").split("\n")
                quoted = ""
                reply = ""
                for line in lines:
                    line = line.lstrip()
                    if not line.startswith("> "):
                        reply += line + "\n"
                    else:
                        quoted += line.lstrip("> ") + "\n"
                quoted = quoted.strip()
                content = self.remove_first_mention(reply).strip()
                quoted_message = await self.get_message_from_content(msg.channel, msg, quoted, msg.raw_mentions)

            else:
                link = Utils.first_word(content)
                user = None
                for mention in msg.raw_mentions:
                    user = mention
                    break
                content = Utils.strip_to_left(content)
                content = content[len(link):]
                content = Utils.strip_to_left(content)

                if user is not None:
                    quoted_message = await self.get_message_from_user(msg.channel, msg, user)
                else:
                    quoted_message = await self.get_message_from_link(link)

            if quoted_message is None:
                return

            quote = quoted_message.content
            author_name = quoted_message.author.display_name
            author_url = "https://discordapp.com/users/" + str(quoted_message.author.id)

            if quoted_message.author.id == msg.guild.me.id:
                for embed in quoted_message.embeds:
                    quote = embed.description
                    author_name = embed.author.name
                    author_url = embed.author.icon_url
                    break

            embed = discord.Embed()
            embed.set_author(
                name=author_name,
                url=author_url,
                icon_url=quoted_message.author.avatar_url)

            link = "https://discordapp.com/channels/{guild_id}/{channel_id}/{message_id}".format(
                guild_id=quoted_message.guild.id,
                channel_id=quoted_message.channel.id,
                message_id=quoted_message.id
            )

            embed.description = "[" + quoted_message.created_at.strftime("%d/%m/%Y, %H:%M:%S") + \
                                "](" + link + ")\n " + quote

            await msg.channel.send(Quoter.mention_user(quoted_message.author), embed=embed)

            if content.lstrip() != "":
                async with aiohttp.ClientSession() as session:
                    async with session.get(str(msg.author.avatar_url)) as resp:
                        if resp.status == 200:
                            content = content.lstrip()
                            await msg.channel.send(
                                self._config.reply_text.format(user=Quoter.mention_user(msg.author), reply=content))

            if msg.guild.me.guild_permissions.manage_messages:
                await msg.delete()
